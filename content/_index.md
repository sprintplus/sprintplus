

![SprintPlus-logo](static/img/logo.png)

Sprint+ is a **bike computer application** that help Client **calculates and displays cycling information**. The application use Client's GPS for calculates the cycling information such as location, speed, distance, weather, etc. then, Sprint+ will record the cycling information and display through mobile application.

Sprint+ is an application developed by Plus IT Solution Co., Ltd.

For more information please visit our website [Plus IT Solution](https://www.plusitsolution.com/).
