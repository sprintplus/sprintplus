---
title: Contact
comments: false
---

### Head Quater
- **Location** : 99/209 SuanLuang BangKho ChomThong Bangkok, Thailand 10150
- **Registration** ID 0105554021041 (HQ)
- **BTS** : Wutthakat station exit 2
- **Tel** : (+66) 95 950 9953
- **Email** : hr@plusitsolution.com
- **Website** : [Plus IT Solution](https://www.plusitsolution.com/)
- **Facebook** : [Facebook](https://www.facebook.com/plusitsolution)

### Development Center
- **Location** : 64/22, Charoen Krung 42/1 Rd, Bangrak, Bangkok, Thailand 10500
- **BTS** : Sapantaksin station exit 1
- **Tel** : (+66) 87 671 2225
- **Email** : contact@plusitsolution.com


### Hours
| Day | Time |
| :------ |:--- |
| Sunday | Closed |
| Monday | 10:00 AM – 6:00 PM |
| Tuesday | 10:00 AM – 6:00 PM |
| Wednesday | 10:00 AM – 6:00 PM |
| Thursday | 10:00 AM – 6:00 PM |
| Friday | 10:00 AM – 6:00 PM |
| Saturday | Closed |
